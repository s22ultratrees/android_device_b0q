#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0

# Enable updating of APEXes
$(call inherit-product, $(SRC_TARGET_DIR)/product/updatable_apex.mk)

TARGET_USES_KERNEL_PLATFORM ?= true



KERNEL_PREBUILT_DIR ?= device/qcom/gki-kernel
KERNEL_PRODUCT_DIR := kernel_obj
KERNEL_MODULES_INSTALL := dlkm
KERNEL_MODULES_OUT ?= $(OUT_DIR)/target/product/$(LINEAGE_BUILD)/$(KERNEL_MODULES_INSTALL)/lib/modules
BOARD_VENDOR_RAMDISK_KERNEL_MODULES_BLOCKLIST_FILE := $(wildcard $(KERNEL_PREBUILT_DIR)/vendor_dlkm/modules.blocklist)
BOARD_VENDOR_KERNEL_MODULES_BLOCKLIST_FILE := $(wildcard $(KERNEL_PREBUILT_DIR)/vendor_dlkm/modules.blocklist)

BOARD_VENDOR_RAMDISK_KERNEL_MODULES += $(first_stage_modules)
BOARD_VENDOR_RAMDISK_KERNEL_MODULES += $(second_stage_modules)

# DTBs
BOARD_PREBUILT_DTBOIMAGE := $(KERNEL_PREBUILT_DIR)/dtbs/dtbo.img
#BOARD_PREBUILT_DTBIMAGE_DIR := $(KERNEL_PREBUILT_DIR)/dtbs/

# Prebuilt kernel
TARGET_PREBUILT_KERNEL := $(KERNEL_PREBUILT_DIR)/Image

# Kernel Headers
TARGET_BOARD_KERNEL_HEADERS := $(KERNEL_PREBUILT_DIR)/kernel-headers

# Kernel
PRODUCT_COPY_FILES += $(KERNEL_PREBUILT_DIR)/Image:kernel
PRODUCT_COPY_FILES += $(KERNEL_PREBUILT_DIR)/System.map:$(KERNEL_PRODUCT_DIR)/System.map


# fastbootd
PRODUCT_PACKAGES += \
    android.hardware.fastboot@1.1-impl-mock \
    fastbootd

# Health
PRODUCT_PACKAGES += \
    android.hardware.health@2.1-impl \
    android.hardware.health@2.1-impl.recovery \
    android.hardware.health@2.1-service

# Inherit virtual_ab_ota product
$(call inherit-product, \
    $(SRC_TARGET_DIR)/product/virtual_ab_ota.mk)

# A/B
PRODUCT_PACKAGES += \
    android.hardware.boot@1.2-impl-qti \
    android.hardware.boot@1.2-impl-qti.recovery \
    android.hardware.boot@1.2-service
    
   PRODUCT_PACKAGES += \
   vendor.samsung.hardware.gnss.ISehGnss@2 \
   vendor.samsung.hardware.hyper.ISehHyPer@2 \
   vendor.samsung.hardware.secuirty.hdcp.wifidisplay.ISehHdcp@2 \

PRODUCT_PACKAGES_DEBUG += \
    bootctl

AB_OTA_POSTINSTALL_CONFIG += \
    RUN_POSTINSTALL_system=true \
    POSTINSTALL_PATH_system=system/bin/otapreopt_script \
    FILESYSTEM_TYPE_system=ext4 \
    POSTINSTALL_OPTIONAL_system=true

AB_OTA_POSTINSTALL_CONFIG += \
    RUN_POSTINSTALL_vendor=true \
    POSTINSTALL_PATH_vendor=bin/checkpoint_gc \
    FILESYSTEM_TYPE_vendor=ext4 \
    POSTINSTALL_OPTIONAL_vendor=true

PRODUCT_PACKAGES += \
  update_engine \
  update_engine_sideload \
  update_verifier

PRODUCT_PACKAGES += \
    checkpoint_gc \
    otapreopt_script

PRODUCT_VIRTUAL_AB_OTA := true

# Inherit virtual_ab_ota product
$(call inherit-product, \
    $(SRC_TARGET_DIR)/product/virtual_ab_ota.mk)

# A/B
PRODUCT_PACKAGES += \
    android.hardware.boot@1.1-impl-qti \
    android.hardware.boot@1.1-impl-qti.recovery \
    android.hardware.boot@1.1-service

PRODUCT_PACKAGES_DEBUG += \
    bootctl

AB_OTA_POSTINSTALL_CONFIG += \
    RUN_POSTINSTALL_system=true \
    POSTINSTALL_PATH_system=system/bin/otapreopt_script \
    FILESYSTEM_TYPE_system=ext4 \
    POSTINSTALL_OPTIONAL_system=true

AB_OTA_POSTINSTALL_CONFIG += \
    RUN_POSTINSTALL_vendor=true \
    POSTINSTALL_PATH_vendor=bin/checkpoint_gc \
    FILESYSTEM_TYPE_vendor=ext4 \
    POSTINSTALL_OPTIONAL_vendor=true

PRODUCT_PACKAGES += \
  update_engine \
  update_engine_sideload \
  update_verifier

PRODUCT_PACKAGES += \
    checkpoint_gc \
    otapreopt_script

PRODUCT_VIRTUAL_AB_OTA := true


# Hardware
PRODUCT_USES_QCOM_HARDWARE := true
PRODUCT_BOARD_PLATFORM := taro

# Overlays
PRODUCT_ENFORCE_RRO_TARGETS := *

# Partitions
PRODUCT_BUILD_SUPER_PARTITION := false
PRODUCT_USE_DYNAMIC_PARTITIONS := true

# Product characteristics
PRODUCT_CHARACTERISTICS := nosdcard

# Rootdir
PRODUCT_PACKAGES += \
    init.qti.kernel.debug-taro.sh \
    init.qcom.efs.sync.sh \
    init.qcom.sensors.sh \
    init.crda.sh \
    init.qti.kernel.debug-diwali.sh \
    install-recovery.sh \
    init.qcom.sh \
    init.qcom.coex.sh \
    init.vendor.sensordebug.sh \
    qca6234-service.sh \
    init.qti.kernel.debug.sh \
    init.qti.media.sh \
    init.kernel.post_boot-taro.sh \
    init.qti.display_boot.sh \
    init.mdm.sh \
    init.qcom.post_boot.sh \
    init.qcom.class_core.sh \
    init.qti.kernel.early_debug.sh \
    init.kernel.post_boot.sh \
    init.class_main.sh \
    init.qcom.sdio.sh \
    init.kernel.post_boot-diwali.sh \
    init.qcom.early_boot.sh \
    init.qcom.usb.sh \
    init.kernel.post_boot-cape.sh \
    init.qti.write.sh \
    vendor_modprobe.sh \
    init.qti.qcv.sh \
    init.qti.kernel.sh \
    init.qti.kernel.early_debug-taro.sh \
    init.qti.kernel.debug-cape.sh \

PRODUCT_PACKAGES += \
    fstab.qcom \
    init.samsung.rc \
    init.qcom.rc \
    init.samsung.display.rc \
    init.samsung.ese.rc \
    init.target.rc \
    init.samsung.bsp.rc \
    init.qti.ufs.rc \
    init.qcom.usb.rc \
    init.qcom.factory.rc \
    init.b0q.rc \
    init.qti.kernel.rc \
    init.samsung.dp.rc \
    init.samsung.power.rc \
    init.samsung.perf.rc \
    init.recovery.samsung.rc \
    init.recovery.qcom.rc \

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/rootdir/etc/fstab.qcom:$(TARGET_COPY_OUT_RAMDISK)/fstab.qcom

# Shipping API level
PRODUCT_SHIPPING_API_LEVEL := 31

# Soong namespaces
PRODUCT_SOONG_NAMESPACES += \
    $(LOCAL_PATH)

#Platform
MSMSTEPPE := taro

# Inherit the proprietary files
$(call inherit-product, vendor/samsung/b0q/b0q-vendor.mk)

# Soong namespaces
PRODUCT_SOONG_NAMESPACES += \
    $(LOCAL_PATH)

# Soong namespaces
PRODUCT_SOONG_NAMESPACES += \
    $(LOCAL_PATH)


	

